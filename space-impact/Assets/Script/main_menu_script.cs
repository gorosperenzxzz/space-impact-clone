using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;  

public class main_menu_script : MonoBehaviour
{
    public void Play() {  
        SceneManager.LoadScene("main");  
    }
    
    public void exitgame() {  
        Debug.Log("exitgame");  
        Application.Quit();  
    }
}
