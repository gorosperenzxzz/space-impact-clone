﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG_music_script : MonoBehaviour
{

    private static BG_music_script backgroundMusic;

    void Awake()
    {
        if(backgroundMusic == null) {
            backgroundMusic = this;
            DontDestroyOnLoad(backgroundMusic);
        }
        else {
            Destroy(gameObject);
        }
    }

}
