﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class playercontrol : MonoBehaviour
{
    public Transform player;
    public KeyCode up;
    public KeyCode down;
    public GameObject bulletPrefab;

    public Text score;
    public int count;
    public GameObject pause, resume, menu;

    // Start is called before the first frame update
    void Start()
    {
         SetCountText();
    }
    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(up)){
            GetComponent<Rigidbody2D> ().velocity = new Vector3 (0,10,0);
        }
        if(Input.GetKeyDown(down)){
            GetComponent<Rigidbody2D> ().velocity = new Vector3 (0,-10,0);
        }
        if((!Input.GetKey(up)) && (!Input.GetKey(down))){
            GetComponent<Rigidbody2D> ().velocity = new Vector3 (0,0,0);
        }
        if(Input.GetKeyDown("space")){
            shootBullet();
        }
        if(Input.GetKeyDown("escape")){
            Time.timeScale = 0;
            pause.gameObject.SetActive(true);
            resume.gameObject.SetActive(true);
            menu.gameObject.SetActive(true);
        }

    }

    public void shootBullet(){
        GameObject b = Instantiate(bulletPrefab) as GameObject;
        b.transform.position = player.transform.position;
    }
    public void SetCountText()
    {
        score.text = "Score: " + count.ToString();
    }
    public void resumeGame()
    {
        Time.timeScale = 1;
        pause.gameObject.SetActive(false);
        resume.gameObject.SetActive(false);
        menu.gameObject.SetActive(false);
    }
    
}